﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace LimitsDatabase
{

    public sealed class LimitsData
    {
      public string LastErrorMessage = string.Empty;

        // data structures for test limits database
      public class limitsItem
        {
            public string name;
            public string type;
            public string operation;
            public string value1;
            public string value2;
            public bool optional;
        }
        public class parametersItem
        {
            public string name;
            public string value;
            public bool optional;
        }

        public class globalsItem
        {
            public string name;
            public string value;
            public string optional;
        }
        public class stepsItem : ICloneable
        {
            public string               guid;
            public string               name;
            public string               function_call;
            public string               do_on_fail;
            public string               do_on_pass;
            public string               test_type;
            public int                  step_number;
            public int                  maxretries;
            public int                  pass_on_x_tries;
            public string               step_operation;
            public List<limitsItem>     limits;
            public List<parametersItem> parameters;

            public stepsItem() { }

            /// <summary>
            /// Get a parameter. Case insensitive
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            public string GetParameterValue(string name)
            {
                string value = string.Empty;
                if (parameters != null)
                {
                    if (parameters.Exists(x => x.name.ToUpper() == name.ToUpper()))
                        value = parameters.Find(x => x.name.ToUpper() == name.ToUpper()).value;
                }
                return value;
            }

            public bool DoesParameterExist(string name)
            {
                return parameters.Exists(x => x.name.ToUpper() == name.ToUpper());
            }

            public limitsItem GetLimitValues(string name)
            {
                limitsItem value = new limitsItem();
                value = limits.Find(x => x.name.ToUpper() == name.ToUpper());
                return value;
            }

            public bool DoesLimitExist(string name)
            {
                return limits.Exists(x => x.name.ToUpper() == name.ToUpper());
            }

            public object Clone()
            {
                stepsItem newstepsItem = (stepsItem)this.MemberwiseClone();

                if (limits != null)
                {
                    List<limitsItem> newlimitsItem = new List<limitsItem>();
                    foreach (var item in limits)
                    {
                        newlimitsItem.Add(item);
                    }
                    newstepsItem.limits = newlimitsItem;
                }
                if (parameters != null)
                {
                    List<parametersItem> newparamsItem = new List<parametersItem>();
                    foreach (var item in parameters)
                    {
                        newparamsItem.Add(item);
                    }
                    newstepsItem.parameters = newparamsItem;
                }


                return newstepsItem;
            }
        }


        public class product_codesItem
        {
            public string guid;
            public string product_code;
            public float version;
            public string function_code;
            public string modify_date;
            public string comment;
            public List<stepsItem> steps;
            public List<globalsItem> globals;
            public string GetGlobalValue(string name)
            {
                string value = string.Empty;
                if (globals != null)
                {
                    foreach (var item in globals)
                    {
                        if (item.name.ToUpper() == name.ToUpper())
                            value = item.value;
                    }
                }
                return value;
            }

        }

        // structures for availiable tests database
        public class avai_testsItem
        {
            public string name;
            public string function;
            public string testType;
            public string description;
            public List<limitsItem> limits;
            public List<parametersItem> parameters;
        }

        private class databaseInfoData
        {
            public string user;
            public string password;
            public string server;
            public string database;
            public string availdatabase;
        }

        private databaseInfoData databaseInfo = new databaseInfoData();
        private string mysqlConnectString = string.Empty;
        private string mysqlAvailDBConnectString = string.Empty;
        private bool disposed = true;

        //------------------------------------------------------------
        // LimitsData
        //  function constructor
        //
        //  Parameters:
        //      string serever - server to connect to
        //      string database - database to connect to
        //      string user - database user to use
        //      string password - password for the user
        //      
        //\
        /// <summary>
        /// Constructor for LimitsData.
        /// </summary>
        /// <param name="server"server to connect to></param>
        /// <param name="database">database to connect to</param>
        /// <param name="user">database user to use</param>
        /// <param name="password">password for the user</param>
        /// <param name="availdatabase">database for availible tests</param>
        public LimitsData(string server, string database, string user, string password, string availdatabase)
        {
            databaseInfo.user = user;
            databaseInfo.password = password;
            databaseInfo.server = server;
            databaseInfo.database = database;
            databaseInfo.availdatabase = availdatabase;
            mysqlConnectString = "Server=" + server + ";Database=" + database + ";uid=" + user + ";password=" + password + ";";
            mysqlAvailDBConnectString = "Server=" + server + ";Database=" + availdatabase + ";uid=" + user + ";password=" + password + ";";
        }

        public MySqlConnection OpenLimitsData()
        {
            MySqlConnection connection = null;
            LastErrorMessage = string.Empty;
            if (databaseInfo.user == string.Empty)
            {
                LastErrorMessage = "Database user has not been defined";
                return null;
            }
            if (databaseInfo.password == string.Empty)
            {
                LastErrorMessage = "Database password has not been defined";
                return null;
            }
            if (databaseInfo.server == string.Empty)
            {
                LastErrorMessage = "Database server has not been defined";
                return null;
            }
            if (databaseInfo.database == string.Empty)
            {
                LastErrorMessage = "Database database has not been defined";
                return null;
            }
            mysqlConnectString = "Server=" + databaseInfo.server + ";Database=" + databaseInfo.database + ";uid=" + databaseInfo.user + ";password=" + databaseInfo.password + ";";

            connection = new MySqlConnection(mysqlConnectString);
            disposed = false;

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening database: " + ex.Message;
                return null;
            }

            return connection;
        }

        //-----------------------------------------------------------
        // Close
        //  Close the open test database
        //
        public void CloseLimitDatabase(MySqlConnection connection)
        {
            if (this.disposed)
            {
                return;     // if disposed, nothing to do
            }

            if (connection != null)
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        public MySqlConnection OpenAvailDatabase()
        {
            MySqlConnection connection = null;
            LastErrorMessage = string.Empty;
            if (databaseInfo.user == string.Empty)
            {
                LastErrorMessage = "Database user has not been defined";
                return null;
            }
            if (databaseInfo.password == string.Empty)
            {
                LastErrorMessage = "Database password has not been defined";
                return null;
            }
            if (databaseInfo.server == string.Empty)
            {
                LastErrorMessage = "Database server has not been defined";
                return null;
            }
            if (databaseInfo.availdatabase == string.Empty)
            {
                LastErrorMessage = "AvailDatabase database has not been defined";
                return null;
            }
            mysqlConnectString = "Server=" + databaseInfo.server + ";Database=" + databaseInfo.availdatabase + ";uid=" + databaseInfo.user + ";password=" + databaseInfo.password + ";";

            connection = new MySqlConnection(mysqlConnectString);
            disposed = false;

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening database: " + ex.Message;
                return null;
            }

            return connection;
        }


        public bool GetListOfProductCodes(ref List<string> codes, ref string errorMessage)
        {
            MySqlCommand  cmd = null;
            MySqlDataReader rdr = null;
            MySqlConnection connection = null;

            connection = OpenLimitsData();
            if (connection == null)
            {
                errorMessage = LastErrorMessage;
                return false;
            }
            if (connection.State != ConnectionState.Open)
            {
                errorMessage = "Database is not open.";
                return false;
            }

            errorMessage = string.Empty;
            codes.Clear();


            cmd = connection.CreateCommand();
            cmd.CommandText = "select product_code, version, function_code, modify_date from product_codes " +
                                        "ORDER BY product_code ASC, version ASC";
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                errorMessage = "Error get product codes: " + ex.Message;
                return false;
            }
            codes.Clear();
            if (rdr.HasRows)
            {
                while(rdr.Read())
                {
                    codes.Add(rdr.GetString(0) + "#" + rdr.GetFloat(1).ToString("N2") + "#" + rdr.GetString(2) + "#" + rdr.GetString(3));
                }
            }
            rdr.Close();
            connection.Close();
            return true;
        }

        public bool GetListOfAvaiTests(ref List<avai_testsItem> tests, ref string errorMessage)
        {
            MySqlCommand cmd = null;
            MySqlDataReader rdr = null;
            MySqlCommand lcmd = null;
            MySqlDataReader lrdr = null;
            //MySqlCommand pcmd = null;
            //MySqlDataReader prdr = null;
            avai_testsItem readItem;
            limitsItem lreadItem;
            parametersItem preadItem;
            MySqlConnection connection = null;
            MySqlConnection lconnection = null;
            string cmdText = string.Empty;

            connection = OpenAvailDatabase();
            if (connection == null)
            {
                errorMessage = LastErrorMessage;
                return false;
            }
            if (connection.State != ConnectionState.Open)
            {
                errorMessage = "Database is not open.";
                return false;
            }

            errorMessage = string.Empty;

            if (connection == null)
            {
                errorMessage = "Database has not been intialized.";
                return false;
            }
            if (connection.State != ConnectionState.Open)
            {
                errorMessage = "Database is not open.";
                return false;
            }

            tests.Clear();

            cmdText = "select name, function_call, test_type, description from tests;";
            cmd = new MySqlCommand(cmdText, connection);
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                errorMessage = "Error getting tests: " + ex.Message;
                return false;
            }
            if (rdr.HasRows)
            {
                // test level
                while (rdr.Read()) 
                {
                    readItem = new avai_testsItem();
                    readItem.name = rdr.GetString(0);
                    readItem.function =rdr.GetString(1);
                    readItem.testType = rdr.GetString(2);
                    readItem.description = rdr.GetString(3);

                    // get any limits defined
                    lconnection = OpenAvailDatabase();

                    cmdText = "SELECT test_id, name, type, operation, value1, value2, optional FROM limits " +
                                "WHERE (test_id = '" + readItem.function + "')";
                    lcmd = new MySqlCommand(cmdText, lconnection);
                    lrdr = lcmd.ExecuteReader();
                    if (lrdr.HasRows)
                    {
                        readItem.limits = new List<limitsItem>();
                        while (lrdr.Read())
                        {
                           lreadItem = new limitsItem();
                            lreadItem.name = lrdr.GetString(1);
                            lreadItem.type = lrdr.GetString(2);
                            lreadItem.operation = lrdr.GetString(3);
                            lreadItem.value1 = lrdr.GetString(4);
                            if (lrdr.IsDBNull(5))              // the second value might be a null value
                                lreadItem.value2 = string.Empty;
                            else
                                lreadItem.value2 = lrdr.GetString(5);
                            lreadItem.optional = lrdr.GetString(6).ToUpper().Equals("YES");
                            readItem.limits.Add(lreadItem);
                        }
                    }
                    lrdr.Close();

                    // get any parameters defined
                    cmdText = "SELECT test_id, name, value, optional FROM parameters " +
                                    "WHERE (test_id = '" + readItem.function + "')";
                    lcmd = new MySqlCommand(cmdText, lconnection);
                    lrdr = lcmd.ExecuteReader();
                    if (lrdr.HasRows)
                    {
                        readItem.parameters = new List<parametersItem>();
                        while (lrdr.Read())
                        {
                            preadItem = new parametersItem();
                            preadItem.name = lrdr.GetString(1);
                            preadItem.value = lrdr.GetString(2);
                            preadItem.optional = lrdr.GetString(3).ToUpper().Equals("YES");
                            readItem.parameters.Add(preadItem);
                        }
                    }
                    lrdr.Close();
                    lconnection.Close();

                    tests.Add(readItem);

                }
            }
            rdr.Close();
            connection.Close();
            return true;
        }

        public bool SaveData(product_codesItem data, string comment, ref string errorMessage)
        {
            Guid product_codes_id;
            Guid steps_id;
            Guid parameters_id;
            Guid limits_id;
            Guid globals_id;
            MySqlCommand cmd = null;
            MySqlConnection connection = null;

            connection = OpenLimitsData();
            if (connection == null)
            {
                errorMessage = LastErrorMessage;
                return false;
            }
            if (connection.State != ConnectionState.Open)
            {
                errorMessage = "Database is not open.";
                return false;
            }


            product_codes_id = Guid.NewGuid();

            // add product_code data
            cmd = connection.CreateCommand();
            cmd.CommandText = "insert into product_codes (id, product_code, version, modify_date, comment, function_code) values " +
                "('" + product_codes_id + "','" + data.product_code + "','" + data.version.ToString("N2") + "', NOW(),'" + comment + "','" + data.function_code + "');";
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            if (data.globals != null)
            {
                foreach (var item in data.globals)
                {
                    globals_id = Guid.NewGuid();
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "insert into globals (id, name, product_codes_id, value) values " +
                        "('" + globals_id + "','" + item.name + "','" + product_codes_id + "','" + item.value +  "');";
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
            }

            if (data.steps != null)
            {
                foreach (var item in data.steps)
                {
                    steps_id = Guid.NewGuid();

                     // add step data
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "insert into steps (id, product_codes_id, name, do_on_fail, do_on_pass, test_type, function_call, step_number, maxretries, pass_on_x_passes, step_operation) values " +
                        "('" + steps_id + "','" + product_codes_id + "','" + item.name + "','" + item.do_on_fail + "','" + item.do_on_pass + "','" + item.test_type + "','" +
                        item.function_call + "','" + item.step_number + "','" + item.maxretries + "','" + item.pass_on_x_tries + "','" + item.step_operation + "');";
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();

                   if (item.limits != null)
                    {
                        foreach (var litem in item.limits)
                        {
                            // add limits data
                            limits_id = Guid.NewGuid();
                            cmd = connection.CreateCommand();
                            cmd.CommandText = "insert into limits (id, steps_id, name, type, operation, value1, value2, optional) values " +
                                "('" + limits_id + "','" + steps_id + "','" + litem.name + "','" + litem.type + "','" + litem.operation + "','" + 
                                      litem.value1 + "','" + litem.value2 + "','" + (litem.optional ? "YES":"NO") + "');";
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                    }
                    if (item.parameters != null)
                    {
                        foreach (var pitem in item.parameters)
                        {
                            // add parameter data
                            parameters_id = Guid.NewGuid();
                            cmd = connection.CreateCommand();
                            cmd.CommandText = "insert into parameters (id, steps_id, name, value, optional) values " +
                                "('" + parameters_id + "','" + steps_id + "','" + pitem.name + "','" + pitem.value + "','" +  (pitem.optional ? "YES":"NO") + "');";
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                    }
                }
            }
            connection.Close();
            return true;
        }

        /// <summary>
        /// read the data for a product code/version/function code.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool GetData (string pc, string ver,  string functionCode, ref product_codesItem data, ref string errorMessage)
        {
            bool status = true;
            //List<stepsItem> steps;
            string cmdText = string.Empty;

            MySqlCommand cmd = null;
            MySqlCommand lcmd = null;
            MySqlDataReader rdr = null;
            MySqlDataReader lrdr = null;
            MySqlConnection connection = null;
            MySqlConnection lconnection = null;

            connection = OpenLimitsData();
            if (connection == null)
            {
                errorMessage = LastErrorMessage;
                return false;
            }
            if (connection.State != ConnectionState.Open)
            {
                errorMessage = "Database is not open.";
                return false;
            }

            // get the code/version top record
            cmdText = "SELECT        id, product_code, version, modify_date, comment, function_code " +
                                    " FROM product_codes " +
                                    " WHERE (product_code = '" + pc + "') AND (version = '" + ver + "') AND (function_code = '" + functionCode + "');";
            cmd = new MySqlCommand(cmdText, connection);
            rdr = cmd.ExecuteReader();
            if (rdr.HasRows)                                                // found data for product
            {
                if (data.steps != null)                                     // get rid of any old stuff
                    data.steps.Clear(); 
                if (data.globals != null)                    
                    data.globals.Clear();
                rdr.Read();                                                 // get the top level data
                data.product_code = rdr[1].ToString();                      // save the product code
                float.TryParse(rdr[2].ToString(), out data.version);        // save the product version
                data.guid = rdr[0].ToString();                              // save the guid for this data
                data.comment = rdr[4].ToString();                           // save comment
                data.modify_date = rdr[3].ToString();                       // save modify date
                data.function_code = rdr[5].ToString();                     // save function code
                rdr.Close();

                cmdText = "SELECT       name, product_codes_id, value " +
                                        "FROM globals " +
                                        " WHERE (product_codes_id = '" + data.guid + "')";
                cmd = new MySqlCommand(cmdText, connection);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)                                        // if there are globals
                {
                    while (rdr.Read())
                    {
                        globalsItem lvglobal = new globalsItem();
                        lvglobal.name = rdr.GetString(0);
                        lvglobal.value = rdr.GetString(2);
                        if (data.globals == null)
                            data.globals = new List<globalsItem>();
                        data.globals.Add(lvglobal);
                    }
                }
                rdr.Close();
                cmd.Dispose();

                    // get the step data
                cmdText = "SELECT        id, product_codes_id, name, do_on_fail, do_on_pass, test_type, function_call, step_number, maxretries, pass_on_x_passes, step_operation " +
                                        "FROM steps " +
                                        "WHERE(product_codes_id = '" + data.guid + "' ) " +
                                        "ORDER BY step_number ASC";
                cmd = new MySqlCommand(cmdText, connection);
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)                                    // if there are steps
                {
                    while (rdr.Read())                              // get the step data
                    {
                        stepsItem lvstep = new stepsItem();
                        lvstep.guid = rdr.GetString(0);
                        lvstep.name = rdr.GetString(2);
                        lvstep.do_on_fail = rdr.GetString(3);
                        lvstep.do_on_pass = rdr.GetString(4);
                        lvstep.test_type = rdr.GetString(5);
                        lvstep.function_call = rdr.GetString(6);
                        lvstep.step_number = rdr.GetInt32(7);
                        lvstep.maxretries = rdr.GetInt32(8);
                        lvstep.pass_on_x_tries = rdr.GetInt32(9);
                        lvstep.step_operation = rdr.GetString(10);

                        // see if there are any limits for this step
                        lconnection = OpenLimitsData();
                        cmdText = "SELECT steps_id, name, type, operation, value1, value2, optional FROM limits WHERE ( steps_id = " + "'" + lvstep.guid + "');";
                        lcmd = new MySqlCommand(cmdText, lconnection);
                        lrdr = lcmd.ExecuteReader();
                        if (lrdr.HasRows)                           // if there are limits
                        {
                            while (lrdr.Read())                     // get the limit data
                            {
                                limitsItem lvlimit = new limitsItem();
                                lvlimit.name = lrdr.GetString(1);
                                lvlimit.type = lrdr.GetString(2);
                                lvlimit.operation = lrdr.GetString(3);
                                lvlimit.value1 = lrdr.GetString(4);
                                lvlimit.value2 = lrdr.GetString(5);
                                lvlimit.optional = lrdr.GetString(6).ToUpper().Equals("YES");
                                if (lvstep.limits == null)
                                    lvstep.limits = new List<limitsItem>();
                                lvstep.limits.Add(lvlimit);
                            }
                        }
                        lrdr.Close();
                        lconnection.Close();

                        // see if there are any parameters for this step
                        lconnection = OpenLimitsData();
                        cmdText = "SELECT steps_id, name, value, optional FROM parameters WHERE ( steps_id = " + "'" + lvstep.guid + "');";
                        lcmd = new MySqlCommand(cmdText, lconnection);
                        lrdr = lcmd.ExecuteReader();
                        if (lrdr.HasRows)                       // if there are parameters
                        {
                            while (lrdr.Read())                 // get the parameters
                            {
                                parametersItem lvparameters = new parametersItem();
                                lvparameters.name = lrdr.GetString(1);
                                lvparameters.value = lrdr.GetString(2);
                                lvparameters.optional = lrdr.GetString(3).ToUpper().Equals("YES");
                                if (lvstep.parameters == null)
                                    lvstep.parameters = new List<parametersItem>();
                                lvstep.parameters.Add(lvparameters);
                            }
                        }
                        lrdr.Close();
                        lconnection.Close();

                        if (data.steps == null)
                            data.steps = new List<stepsItem>();
                        data.steps.Add(lvstep);
                    }
                }
                rdr.Close();
                connection.Close();
           }
            else
            {
                errorMessage = "product " + pc + "/" + ver + "/" + functionCode + " does not exist";
                return false;
            }

            return status;
        }

        public bool DeleteData(string productCode, string version, string functionCode, ref string errorMessage)
        {
            bool status = true;
            string errorMsg = string.Empty;
            string procduct_code_id = string.Empty;
            string cmdText = string.Empty;
            LimitsData.product_codesItem data = new product_codesItem();
            MySqlConnection connection = null;
            MySqlCommand cmd = null;
            MySqlDataReader rdr;

            GetData(productCode, version, functionCode, ref data, ref errorMsg);
            if (errorMsg != string.Empty)
            {
                errorMessage = errorMsg;
                return false;
            }

            connection = OpenLimitsData();
            if (connection == null)
            {
                errorMessage = LastErrorMessage;
                return false;
            }
            if (connection.State != ConnectionState.Open)
            {
                errorMessage = "Database is not open.";
                return false;
            }

            // step through the steps
            foreach (var item in data.steps)
            {
                // delete the limits
                cmdText = "DELETE FROM limits WHERE steps_id = '" + item.guid + "';";
                // delete the parameters
                cmd = new MySqlCommand(cmdText, connection);
                rdr = cmd.ExecuteReader();
                rdr.Close();
                cmdText = "DELETE FROM parameters WHERE steps_id = '" + item.guid + "';";
                // delete the parameters
                cmd = new MySqlCommand(cmdText, connection);
                rdr = cmd.ExecuteReader();
                rdr.Close();

            }
            // delete the step
            cmdText = "DELETE FROM steps WHERE product_codes_id = '" + data.guid + "';";
            cmd = new MySqlCommand(cmdText, connection);
            rdr = cmd.ExecuteReader();
            rdr.Close();

            // delete the globals
            cmdText = "DELETE FROM globals WHERE product_codes_id = '" + data.guid + "';";
            cmd = new MySqlCommand(cmdText, connection);
            rdr = cmd.ExecuteReader();
            rdr.Close();

            // delete top
            cmdText = "DELETE FROM product_codes WHERE id = '" + data.guid + "';";
            cmd = new MySqlCommand(cmdText, connection);
            rdr = cmd.ExecuteReader();
            rdr.Close();

            return status;
        }
    }
}
